.PHONY: all
all:
	@$(MAKE) -s -C apr
	@$(MAKE) -s -C asio
	@$(MAKE) -s -C cpputest
	@$(MAKE) -s -C cpr
	@$(MAKE) -s -C curl
	@$(MAKE) -s -C date
	@$(MAKE) -s -C doctest
	@$(MAKE) -s -C hiredis
	@$(MAKE) -s -C httplib
	@$(MAKE) -s -C json
	@$(MAKE) -s -C jwt-cpp
	@$(MAKE) -s -C kashmir
	@$(MAKE) -s -C openssl
	@$(MAKE) -s -C pq
	@$(MAKE) -s -C pqxx
	@$(MAKE) -s -C pstreams
	@$(MAKE) -s -C recycle
	@$(MAKE) -s -C restbed
	@$(MAKE) -s -C spdlog
	@$(MAKE) -s -C sqlpp11
	@$(MAKE) -s -C yaml

.PHONY: clean
clean:
	@$(MAKE) -s -C apr clean
	@$(MAKE) -s -C asio clean
	@$(MAKE) -s -C cpputest clean
	@$(MAKE) -s -C cpr clean
	@$(MAKE) -s -C curl clean
	@$(MAKE) -s -C date clean
	@$(MAKE) -s -C doctest clean
	@$(MAKE) -s -C hiredis clean
	@$(MAKE) -s -C httplib clean
	@$(MAKE) -s -C json clean
	@$(MAKE) -s -C jwt-cpp clean
	@$(MAKE) -s -C kashmir clean
	@$(MAKE) -s -C openssl clean
	@$(MAKE) -s -C pq clean
	@$(MAKE) -s -C pqxx clean
	@$(MAKE) -s -C pstreams clean
	@$(MAKE) -s -C recycle clean
	@$(MAKE) -s -C restbed clean
	@$(MAKE) -s -C spdlog clean
	@$(MAKE) -s -C sqlpp11 clean
	@$(MAKE) -s -C yaml clean
